# Assumptions
* I changed the rotation Prime start time from 3pm to 4pm. Previously, it was overlapping with the
afternoon timezone. I didn't know how to account for this and keep the rotations discrete.
* The project tests are to be executed using module mode: `python3 -m unittest`
* The project prints the reports to the command line. The project is executed using a simple command `python3 main.py
` (No args are passed). Data is placed in the /resources directory.
* Data is small, so Lists are used to pass data.
* Readability (hopefully) is favored over performance, so lists are iterated over more than the most efficient solution.

# Design
I didn't use object orient design (although I am throwing exceptions). This seemed easier with functions and data classes.

- `reader.py` reads data from local disk and performs parsing. Can raise critical exceptions.
- `transformer.py` combines the two data sources. Can raise critical exceptions.
- `reducer.py` is passed clean data. It generates the CPV reports.
- `main.py` integrates the 3 layers.

If there are data integrity problems, the process blows up until the raw data is fixed. This is easier than setting up a railroad pattern for logging errors (or writing errors to disk) and proceeding.

# Run

Run main script:
`python3 main.py`

Run tests:
`python3 -m unittest -v`

---
# Backend Engineer Coding Exercise

This is a quick coding exercise that we give to our potential
hires.  It is intended to make sure people can write code and also
provides valuable insight into how they think about and solve problems.

## Timeboxing

We want to be respectful of your time, so any work on this should be
timeboxed to several hours, tops.

**Note: Please do _only_ the Backend Engineer exercise. :)**

## Description

Write a program in the language of your choosing that:

 * Consumes both `rotations.csv` and `spots.csv`
 * Generates output that shows cost per view by two dimensions:
   * CPV by creative
   * CPV by rotation by day

**No Jupyter notebooks or `pandas` or `numpy`, please!**

We love these tools here at <redacted company name> but this assignment is about seeing how you solve problems with your code.
Show us your best software engineering skills, not how easy it is to do it with `df.groupby()`. :)

## Details

First, this is intentionally a little underspecified.  Feel free to
ask questions, or to just make assumptions.  If you *do* make any
assumptions for the sake of expediency, document them so we know you
considered them!

For the purpose of this exercise, here's some terminology:

 * "Creative" - Business lingo for a TV ad
 * "Rotation" - The timerange on a TV network that an ad airs in

## Submission

When finished, please submit your solution as a ZIP file to the Greenhouse link we
provided in the coding exercise e-mail. Also, we politely ask that you do not post your solution
in a public repository. Thanks!

## Questions?

If anything comes up, please don't hesitate to ask questions of your interviewer.
