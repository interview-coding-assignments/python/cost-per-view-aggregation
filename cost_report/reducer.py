from dataclasses import dataclass
from datetime import date
from decimal import Decimal
from typing import List, Dict, TypeVar, Tuple

from .transformer import EnrichedSpot


"""
`reducer.py` calculates CPV based on dimensions to generate reports. The input data is cleaned, so functions in this
file should not raise exceptions.
"""

@dataclass
class TotalAccumulator:
    total_spend: Decimal
    total_views: int


# Generic used for dimension keys.
K = TypeVar('K', str, Tuple[str, date])


def calculate_cpv_by_rotation_by_day(spots: List[EnrichedSpot]) -> Dict[Tuple[str, date], Decimal]:
    # Build up an accumulated map by rotation
    rotation_accumulator_dict: Dict[Tuple[str, date], TotalAccumulator] = {}
    for spot in spots:
        key = (spot.rotation_name.value, spot.date)
        _accumulate(rotation_accumulator_dict, spot, key)
    return _cost_per_value_by_dimension(rotation_accumulator_dict)


def calculate_cpv_by_creative(spots: List[EnrichedSpot]) -> Dict[str, Decimal]:
    # Build up an accumulated map by creative
    creative_accumulator_dict: Dict[str, TotalAccumulator] = {}
    for spot in spots:
        key = spot.creative
        _accumulate(creative_accumulator_dict, spot, key)
    return _cost_per_value_by_dimension(creative_accumulator_dict)


# Untested, side-effecting.
def print_creative_report(cpv: Dict[str, Decimal]) -> None:
    print("Cost per View by Creative")
    for k, v in cpv.items():
        rounded_down_cpv = "{:.2f}".format(v)
        print(f"Creative: {k}\t CPV: {rounded_down_cpv}")


# Untested, side-effecting.
def print_rotation_by_date_report(cpv: Dict[Tuple[str, date], Decimal]) -> None:
    print("Cost per View by Rotation by Date")
    for k, v in cpv.items():
        rounded_down_cpv = "{:.2f}".format(v)
        date_str = k[1].strftime("%d/%m/%Y")
        print(f"Rotation: {k[0]}\tDate:{date_str}\t CPV: {rounded_down_cpv}")


def _cost_per_value_by_dimension(acc_dict: Dict[K, TotalAccumulator]) -> Dict[K, Decimal]:
    cpv_dict = {}
    for k, v in acc_dict.items():
        cpv_dict[k] = v.total_spend / v.total_views
    return cpv_dict


def _accumulate(acc: Dict[K, TotalAccumulator], spot: EnrichedSpot, key: K):
    maybe_previous_accumulator = acc.get(key)
    if maybe_previous_accumulator:
        acc_ta = TotalAccumulator(maybe_previous_accumulator.total_spend + spot.spend,
                                  maybe_previous_accumulator.total_views + spot.views)
        acc[key] = acc_ta
    else:
        acc[key] = TotalAccumulator(spot.spend, spot.views)
