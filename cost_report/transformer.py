from dataclasses import dataclass
from datetime import date
from decimal import Decimal
from typing import List

from .reader import RotationName, Spot, Rotation

"""
`transformer.py` joins the datasets together to enrich the spots. The output can be fed to the reporter.
Functions raise exceptions to kill the process if the data is malformed.
"""


class TransformerException(Exception):
    def __init__(self, input_message: str, spot: Spot):
        self.message = f"{input_message}\nInput spot: {spot}"
        super().__init__(self.message)


# This dataclass is almost identical to Spot, but it breaks up a concern. Parsing vs. transforming data.
@dataclass
class EnrichedSpot:
    date: date
    rotation_name: RotationName
    creative: str
    spend: Decimal
    views: int


def enrich_spots(spots: List[Spot], rotations: List[Rotation]) -> List[EnrichedSpot]:
    # Note- Using List here since it keeps the project simple and the data set is small.
    # This function can throw a TransformerException. If there is a single bad data transformation, blow up the process.
    return list(
        map(
            lambda s: EnrichedSpot(
                s.date,
                time_to_rotation(s, rotations),
                s.creative,
                s.spend,
                s.views
            ),
            spots))


def time_to_rotation(spot: Spot, rotations: List[Rotation]) -> RotationName:
    # Throws an iterator error
    # Assumption that rotation time is calculated as [Start, End).
    maybe_rotation = next(filter(lambda r: r.end > spot.time >= r.start, rotations), None)
    if maybe_rotation:
        return maybe_rotation.name
    else:
        raise TransformerException("Out of range spot time. Could not map to a rotation.", spot)
