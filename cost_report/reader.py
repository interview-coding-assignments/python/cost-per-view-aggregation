import csv
from dataclasses import dataclass
from datetime import time, date, datetime
from enum import Enum
from typing import List
from decimal import Decimal

"""
`reader.py` reads CSV files from memory and parses CSV lines into domain object types. Data type validation is performed
on CSV elements. Functions raise exceptions to kill the process if the data is malformed.
"""

EXPECTED_ROTATION_HEADERS = ["Start", "End", "Name"]
EXPECTED_SPOTS_HEADERS = ["Date", "Time", "Creative", "Spend", "Views"]


class RotationName(Enum):
    MORNING = "Morning"
    AFTERNOON = "Afternoon"
    PRIME = "Prime"


class CsvLineParseException(Exception):
    def __init__(self, input_message: str, csv: List[str]):
        self.message = f"{input_message}\nInput CSV string: {csv}"
        super().__init__(self.message)


@dataclass
class Rotation:
    start: time
    end: time
    name: RotationName


@dataclass
class Spot:
    date: date
    time: time
    creative: str
    spend: Decimal
    views: int


class InvalidHeader(Exception):
    def __init__(self, expected_headers: List[str], actual_headers: List[str]):
        self.message = f"Unexpected headers.\nExpected: {expected_headers}\nActual: {actual_headers}"
        super().__init__(self.message)


def load_rotations_csv(path: str, expected_headers: List[str]) -> List[Rotation]:
    """
    Module entry point.

    Loads and validates rotations. Raises exceptions to crash the program eagerly if the rotation data is not as expected.
    Exceptions are raised for malformed rows or unexpected RotationNames (i.e. the code may need to be updated to support
    new rotations).

    CSV headers are injected for testability.

    :param path: Local path to rotation csv file.
    :param expected_headers: Expected CSV headers in order.
    :return: List of validated Rotation objects.
    """
    parsed_rotations = []

    with open(path) as f:
        reader = csv.reader(f)
        for i, row in enumerate(reader):
            # Validate headers. Stop processing the file if headers are not an exact match.
            if i == 0 and row != expected_headers:
                raise InvalidHeader(expected_headers, row)

            # Parse non-header rows. Raise an exception to stop processing the file if there is a malformed line.
            # Rotations are important for the report- continuing with incomplete data would be confusing downstream.
            if i > 0:
                try:
                    parsed_rotation = parse_rotation_row(row, len(expected_headers))
                    parsed_rotations.append(parsed_rotation)
                except CsvLineParseException as e:
                    raise e
        if len(parsed_rotations) != len(RotationName):
            raise ValueError(
                "Rotations in rotation file did not match the number of rotations modeled in RotationName.")
    return parsed_rotations


def parse_rotation_row(row: List[str], expected_length: int) -> Rotation:
    """
    :param row: A list of strings of CSV line elements.
    :param expected_length: Expected integer length of the row.
    :return: Rotation data class object.
    """
    if len(row) != expected_length:
        raise CsvLineParseException("Expected column count did not match actual column count.", row)
    # All mappings are in one place. Use explicit indices rather than trying to map a header to a row.
    start_str = row[0]
    end_str = row[1]
    rotation_str = row[2]
    try:
        # Parse times
        start_time = datetime.strptime(start_str, "%I:%M %p").time()
        end_time = datetime.strptime(end_str, "%I:%M %p").time()
        if start_time >= end_time:
            raise ValueError("Rotation start time is equal to or after rotation end time.")

        # Parse Rotations Name and build domain object.
        if rotation_str == RotationName.MORNING.value:
            return Rotation(start_time, end_time, RotationName.MORNING)
        elif rotation_str == RotationName.AFTERNOON.value:
            return Rotation(start_time, end_time, RotationName.AFTERNOON)
        if rotation_str == RotationName.PRIME.value:
            return Rotation(start_time, end_time, RotationName.PRIME)
        else:
            raise ValueError(f"Unsupported rotation {rotation_str}")
    except Exception as e:
        raise CsvLineParseException(f"Error parsing csv line {e}", row)


def load_spots_csv(path: str, expected_headers: List[str]) -> List[Spot]:
    """
    Module entry point.

    Loads and validates spots. Raises exceptions to crash the program eagerly if the spot data is not as expected.
    Exceptions are raised for malformed rows or unexpected spotNames (i.e. the code may need to be updated to support
    new spots).

    CSV headers are injected for testability.

    :param path: Local path to spot csv file.
    :param expected_headers: Expected CSV headers in order.
    :return: List of validated spot objects.
    """
    parsed_spots = []

    with open(path) as f:
        reader = csv.reader(f)
        for i, row in enumerate(reader):
            # Validate headers. Stop processing the file if headers are not an exact match.
            if i == 0 and row != expected_headers:
                raise InvalidHeader(expected_headers, row)

            # Parse non-header rows. Raise an exception to stop processing the file if there is a malformed line.
            # spots are important for the report- continuing with incomplete data would be confusing downstream.
            if i > 0:
                try:
                    parsed_spot = parse_spot_row(row, len(expected_headers))
                    parsed_spots.append(parsed_spot)
                except CsvLineParseException as e:
                    raise e
    return parsed_spots


def parse_spot_row(row: List[str], expected_length: int) -> Spot:
    """
    :param row: A list of strings of CSV line elements.
    :param expected_length: Expected nteger length of the row.
    :return: Rotation data class object.
    """
    if len(row) != expected_length:
        raise CsvLineParseException("Expected column count did not match actual column count.", row)
    # All mappings are in one place. Use explicit indices rather than trying to map a header to a row.
    date_str = row[0]
    time_str = row[1]
    creative = row[2]
    spend_str = row[3]
    views_str = row[4]
    # Fail fast validation. The first failing validation will raise an error.
    try:
        # Assuming the input format of 01/02/2016 is DD/mm/YYYY
        parsed_date = datetime.strptime(date_str, "%d/%m/%Y").date()
        parsed_time = datetime.strptime(time_str, "%I:%M %p").time()
        # Raise an exception for all empty strings.
        if len(creative.strip()) == 0:
            raise CsvLineParseException("Empty creative string.", row)
        # FIXME This is permissive. I don't want to spend time validating the money string.
        parsed_spend = Decimal(spend_str)
        parsed_views = int(views_str)
        return Spot(parsed_date, parsed_time, creative, parsed_spend, parsed_views)
    except Exception as e:
        raise CsvLineParseException(f"Error parsing csv line {e}", row)

