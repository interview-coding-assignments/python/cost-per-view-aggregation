import os
import unittest
from datetime import date
from decimal import Decimal

import main


class TestReader(unittest.TestCase):
    test_dir_path = os.path.join(os.path.dirname(__file__), "resources")
    expected_rotations_field_count = 3
    expected_spots_field_count = 5

    def test_main_happy_path(self):
        rotations_path = os.path.join(self.test_dir_path, "valid_rotations_test.csv")
        spots_path = os.path.join(self.test_dir_path, "valid_spots_test.csv")

        actual_result = main.read_transform_report(rotations_path, spots_path)
        expected_result = main.Result(
            cpv_by_creative={
                'TEST001H': Decimal('2.809615384615384615384615385'),
                'TEST002H': Decimal('3.404255319148936170212765957')
            },
            cpv_by_rotation_by_day={
                ('Morning', date(2016, 2, 1)): Decimal('1.719047619047619047619047619'),
                ('Afternoon', date(2016, 2, 1)): Decimal('4.642857142857142857142857143'),
                ('Morning', date(2016, 2, 2)): Decimal('3.5'),
                ('Prime', date(2016, 2, 2)): Decimal('2.333333333333333333333333333')
            }
        )

        self.assertEqual(expected_result, actual_result)

    # This is an integration test, so only one type of exception is tested.
    def test_main_exits_program_when_exception_is_raised(self):
        invalid_rotations_path = os.path.join(self.test_dir_path, "invalid_rotations_missing_rotation_test.csv")
        spots_path = os.path.join(self.test_dir_path, "valid_spots_test.csv")

        with self.assertRaises(ValueError):
            main.read_transform_report(invalid_rotations_path, spots_path)
