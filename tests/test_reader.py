import os
import unittest

from cost_report.reader import *


class TestReader(unittest.TestCase):
    test_dir_path = os.path.join(os.path.dirname(__file__), "resources")
    expected_rotations_field_count = 3
    expected_spots_field_count = 5

    def test_reader_loads_clean_data(self):
        rotations_path = os.path.join(self.test_dir_path, "valid_rotations_test.csv")
        decoded_rotations = load_rotations_csv(rotations_path, EXPECTED_ROTATION_HEADERS)

        spots_path = os.path.join(self.test_dir_path, "valid_spots_test.csv")
        decoded_spots = load_spots_csv(spots_path, EXPECTED_SPOTS_HEADERS)

        expected_rotations = [
            Rotation(start=time(6, 0, 0), end=time(12, 0, 0), name=RotationName.MORNING),
            Rotation(start=time(12, 0, 0), end=time(16, 0, 0), name=RotationName.AFTERNOON),
            # I changed the start time of prime in the data as I didn't understand how rotations could overlap. It was
            # originally 3PM.
            Rotation(start=time(16, 0, 0), end=time(20, 0, 0), name=RotationName.PRIME)
        ]
        expected_spots = [
            Spot(date=date(2016, 2, 1), time=time(8, 30), creative='TEST001H', spend=Decimal('120.50'), views=100),
            Spot(date=date(2016, 2, 1), time=time(11, 30), creative='TEST001H', spend=Decimal('240.50'), views=110),
            Spot(date=date(2016, 2, 1), time=time(15, 30), creative='TEST002H', spend=Decimal('500'), views=80),
            Spot(date=date(2016, 2, 1), time=time(15, 34), creative='TEST002H', spend=Decimal('400'), views=90),
            Spot(date=date(2016, 2, 1), time=time(15, 40), creative='TEST001H', spend=Decimal('400'), views=110),
            Spot(date=date(2016, 2, 2), time=time(7, 30), creative='TEST001H', spend=Decimal('700'), views=200),
            Spot(date=date(2016, 2, 2), time=time(19, 30), creative='TEST002H', spend=Decimal('700'), views=300),
        ]

        # Checks for element equality, disregarding order
        self.assertCountEqual(expected_rotations, decoded_rotations)
        self.assertCountEqual(expected_spots, decoded_spots)

    def test_load_rotations_csv_raises_exception_for_unexpected_headers(self):
        rotations_path = os.path.join(self.test_dir_path, "valid_rotations_test.csv")
        bad_headers = ['bad']
        with self.assertRaises(InvalidHeader):
            load_rotations_csv(rotations_path, bad_headers)

    def test_load_rotations_csv_raises_exception_for_unexpected_rotation_count(self):
        rotations_path = os.path.join(self.test_dir_path, "invalid_rotations_missing_rotation_test.csv")
        with self.assertRaises(ValueError):
            load_rotations_csv(rotations_path, EXPECTED_ROTATION_HEADERS)

    def test_load_spots_csv_raises_exception_for_unexpected_headers(self):
        spots_path = os.path.join(self.test_dir_path, "valid_spots_test.csv")
        bad_headers = ['bad']
        with self.assertRaises(InvalidHeader):
            load_spots_csv(spots_path, bad_headers)

    def test_parse_rotation_row_unexpected_field_count(self):
        bad_row_count = ["6:00 AM", "12:00 PM", "Morning"]
        with self.assertRaises(CsvLineParseException):
            parse_rotation_row(bad_row_count, 4)

    def test_parse_rotation_row_missing_data(self):
        bad_row_count = ["6:00 AM", "12:00 PM"]
        with self.assertRaises(CsvLineParseException):
            parse_rotation_row(bad_row_count, self.expected_rotations_field_count)

    def test_parse_rotation_row_parse_errors(self):
        missing_am = ["6:00", "12:00 PM", "Morning"]
        with self.assertRaises(CsvLineParseException):
            parse_rotation_row(missing_am, self.expected_rotations_field_count)

        out_of_range_time = ["6:00 AM", "15:00 PM", "Morning"]
        with self.assertRaises(CsvLineParseException):
            parse_rotation_row(out_of_range_time, self.expected_rotations_field_count)

        bad_rotation = ["6:00 AM", "12:00 PM", "BAD_ROTATION"]
        with self.assertRaises(CsvLineParseException):
            parse_rotation_row(bad_rotation, self.expected_rotations_field_count)

        start_after_end = ["6:00 PM", "12:00 PM", "Morning"]
        with self.assertRaises(CsvLineParseException):
            parse_rotation_row(start_after_end, self.expected_rotations_field_count)

    def test_parse_spot_row_unexpected_field_count(self):
        bad_row_count = ["01/02/2016", "8:30 AM", "TEST001H", "120.50", "100"]
        with self.assertRaises(CsvLineParseException):
            parse_spot_row(bad_row_count, 6)

    def test_parse_spot_row_missing_data(self):
        bad_row_count = ["01/02/2016", "8:30 AM", "TEST001H", "120.50"]
        with self.assertRaises(CsvLineParseException):
            parse_spot_row(bad_row_count, self.expected_spots_field_count)

    def test_parse_spot_row_parse_errors(self):
        date_parse_error = ["100/02/2016", "8:30 AM", "TEST001H", "120.50", "100"]
        with self.assertRaises(CsvLineParseException):
            parse_spot_row(date_parse_error, self.expected_spots_field_count)

        time_parse_error = ["01/02/2016", "8:99 AM", "TEST001H", "120.50", "100"]
        with self.assertRaises(CsvLineParseException):
            parse_spot_row(time_parse_error, self.expected_spots_field_count)

        empty_creative = ["01/02/2016", "8:30 AM", "    ", "120.50", "100"]
        with self.assertRaises(CsvLineParseException):
            parse_spot_row(empty_creative, self.expected_spots_field_count)

        spend_parse_error = ["01/02/2016", "8:30 AM", "TEST001H", "not a decimal", "100"]
        with self.assertRaises(CsvLineParseException):
            parse_spot_row(spend_parse_error, self.expected_spots_field_count)

        view_parse_error = ["01/02/2016", "8:30 AM", "TEST001H", "120.50", "not an int"]
        with self.assertRaises(CsvLineParseException):
            parse_spot_row(view_parse_error, self.expected_spots_field_count)

