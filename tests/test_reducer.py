import unittest
from datetime import date
from decimal import Decimal

from cost_report.reader import RotationName
from cost_report.reducer import calculate_cpv_by_rotation_by_day, calculate_cpv_by_creative
from cost_report.transformer import EnrichedSpot


class TestReducer(unittest.TestCase):
    """
    Reducer doesn't throw exceptions, so only the happy path is tested.
    """

    enriched_spots = [
        EnrichedSpot(date=date(2016, 2, 1), rotation_name=RotationName.MORNING, creative='TEST001H',
                     spend=Decimal('100.00'), views=100),
        EnrichedSpot(date=date(2016, 2, 1), rotation_name=RotationName.MORNING, creative='TEST001H',
                     spend=Decimal('200.00'), views=50),
        EnrichedSpot(date=date(2016, 2, 1), rotation_name=RotationName.AFTERNOON, creative='TEST002H',
                     spend=Decimal('800'), views=100),
        EnrichedSpot(date=date(2016, 2, 1), rotation_name=RotationName.AFTERNOON, creative='TEST002H',
                     spend=Decimal('400'), views=200),
        EnrichedSpot(date=date(2016, 2, 1), rotation_name=RotationName.AFTERNOON, creative='TEST001H',
                     spend=Decimal('400'), views=100),
        EnrichedSpot(date=date(2016, 2, 2), rotation_name=RotationName.MORNING, creative='TEST001H',
                     spend=Decimal('200'), views=200),
        EnrichedSpot(date=date(2016, 2, 2), rotation_name=RotationName.PRIME, creative='TEST002H',
                     spend=Decimal('1000'),
                     views=200),
    ]

    def test_calculate_cpv_by_rotation_by_day(self):
        result = calculate_cpv_by_rotation_by_day(self.enriched_spots)

        expected_result = {
            ('Afternoon', date(2016, 2, 1)): Decimal('4'),
            ('Morning', date(2016, 2, 1)): Decimal('2.00'),
            ('Morning', date(2016, 2, 2)): Decimal('1'),
            ('Prime', date(2016, 2, 2)): Decimal('5'),
        }
        self.assertEqual(expected_result, result)

    def test_calculate_cpv_by_creative(self):
        result = calculate_cpv_by_creative(self.enriched_spots)
        expected_result = {
            'TEST001H': Decimal('2.00'),
            'TEST002H': Decimal('4.4'),
        }
        self.assertEqual(expected_result, result)
