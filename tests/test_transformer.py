import unittest
from datetime import time, date
from decimal import Decimal

from cost_report.reader import Spot, Rotation, RotationName
from cost_report.transformer import time_to_rotation, TransformerException, enrich_spots, EnrichedSpot


class TestTransformer(unittest.TestCase):
    valid_rotations = [
        Rotation(start=time(6, 0, 0), end=time(12, 0, 0), name=RotationName.MORNING),
        Rotation(start=time(12, 0, 0), end=time(16, 0, 0), name=RotationName.AFTERNOON),
        # I changed the start time of prime in the data as I didn't understand how rotations could overlap. It was
        # originally 3PM.
        Rotation(start=time(16, 0, 0), end=time(20, 0, 0), name=RotationName.PRIME)
    ]

    def test_time_to_rotation_happy_path(self):
        morning_spot = Spot(date=date(2016, 2, 1), time=time(8, 30), creative='TEST001H', spend=Decimal('120.50'),
                            views=100)
        actual_rotation = time_to_rotation(morning_spot, self.valid_rotations)
        self.assertEqual(RotationName.MORNING, actual_rotation)

        afternoon_spot = Spot(date=date(2016, 2, 1), time=time(14, 30), creative='TEST001H', spend=Decimal('120.50'),
                              views=100)
        actual_rotation = time_to_rotation(afternoon_spot, self.valid_rotations)
        self.assertEqual(RotationName.AFTERNOON, actual_rotation)

        prime_spot = Spot(date=date(2016, 2, 1), time=time(17, 30), creative='TEST001H', spend=Decimal('120.50'),
                          views=100)
        actual_rotation = time_to_rotation(prime_spot, self.valid_rotations)
        self.assertEqual(RotationName.PRIME, actual_rotation)

    def test_time_to_rotation_with_out_of_range_spot(self):
        spot_1am = Spot(date=date(2016, 2, 1), time=time(1, 30), creative='TEST001H', spend=Decimal('120.50'),
                        views=100)
        with self.assertRaises(TransformerException):
            time_to_rotation(spot_1am, self.valid_rotations)

    def test_enrich_spots_happy_path(self):
        input_spots = [
            Spot(date=date(2016, 2, 1), time=time(8, 30), creative='TEST001H', spend=Decimal('120.50'), views=100),
            Spot(date=date(2016, 2, 1), time=time(15, 30), creative='TEST002H', spend=Decimal('500'), views=80),
            Spot(date=date(2016, 2, 2), time=time(19, 30), creative='TEST002H', spend=Decimal('700'), views=300),
        ]

        enriched_spots = enrich_spots(input_spots, self.valid_rotations)

        expected_enriched_spots = [
            EnrichedSpot(date=date(2016, 2, 1), rotation_name=RotationName.MORNING, creative='TEST001H',
                         spend=Decimal('120.50'), views=100),
            EnrichedSpot(date=date(2016, 2, 1), rotation_name=RotationName.AFTERNOON, creative='TEST002H',
                         spend=Decimal('500'), views=80),
            EnrichedSpot(date=date(2016, 2, 2), rotation_name=RotationName.PRIME, creative='TEST002H',
                         spend=Decimal('700'), views=300),
        ]
        # Checks for element equality, disregarding order
        self.assertCountEqual(expected_enriched_spots, enriched_spots)

    def test_enrich_spots_raises_exceptions(self):
        input_spots = [
            # Time to rotation mapping error
            Spot(date=date(2016, 2, 1), time=time(23, 30), creative='TEST001H', spend=Decimal('120.50'), views=100)
        ]
        with self.assertRaises(TransformerException):
            enrich_spots(input_spots, self.valid_rotations)