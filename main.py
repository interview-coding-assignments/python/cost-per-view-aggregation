import os
from dataclasses import dataclass
from datetime import date
from decimal import Decimal
from typing import Dict, Tuple

import cost_report.reader as reader
import cost_report.reducer as reducer
import cost_report.transformer as transformer


def main():
    # Specify the paths here instead of using arguments. Makes the program easier to run.
    file_path = os.path.join(os.path.dirname(__file__))
    rotations_path = os.path.join(file_path, "resources", "rotations.csv")
    spots_path = os.path.join(file_path, "resources", "spots.csv")
    print(f"Reading data from: {rotations_path}, {spots_path}\n")

    result = read_transform_report(rotations_path, spots_path)

    print("Successfully generated reports. Printing reports.\n")
    reducer.print_creative_report(result.cpv_by_creative)
    print()
    reducer.print_rotation_by_date_report(result.cpv_by_rotation_by_day)


@dataclass
class Result:
    cpv_by_creative: Dict[str, Decimal]
    cpv_by_rotation_by_day: Dict[Tuple[str, date], Decimal]


def read_transform_report(rotations_path: str, spots_path: str) -> Result:
    """
    Helper function used. Side-effect free function used in `main.py` and to make testing easier.

    :param rotations_path: Local file path to rotations csv.
    :param spots_path: Local file path to spots csv.
    :return: Result- data reports to be printed in main.
    """
    # Reader functions can raise exceptions. Kill the process if a single exception is encountered.
    rotations = reader.load_rotations_csv(rotations_path, reader.EXPECTED_ROTATION_HEADERS)
    spots = reader.load_spots_csv(spots_path, reader.EXPECTED_SPOTS_HEADERS)

    # Transformer function can raise exceptions. Kill the process if a single exception is encountered.
    enriched_spots = transformer.enrich_spots(spots, rotations)

    # Happy path once this stage is reached.
    return Result(
        cpv_by_creative=reducer.calculate_cpv_by_creative(enriched_spots),
        cpv_by_rotation_by_day=reducer.calculate_cpv_by_rotation_by_day(enriched_spots)
    )


if __name__ == "__main__":
    main()
